#!/usr/bin/env python3.0
#-*- coding: utf-8 -*-
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
# USA.

"""torest

A bash filter that has to be used with ``noweb`` to extract the
documentation part of a noweb literate program when the documentation
itself is written in restructuredText.

Usage:

Create a bash script with the following lines:

  $ NW = /usr/lib/noweb
  $ $NW/markup <progname.nw> | $NW/finduses | $NW/noidx | ./torest.py > <docname.rst>

For the command-line options of ``markup``, ``finduses`` and ``noidx``
read ``The noweb Hacker’s Guide`` at https://www.cs.tufts.edu/~nr/noweb/guide.ps

Copyright (C) 2016 Sam Nirvana <samnirvana@linuxmail.org>

License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

Written by Sam Nirvana
"""

__scriptname__ = "torest"
__author__ = "Sam Nirvana <samnirvana@linuxmail.org>"
__version__ = "$Revision: 0.0.3 $"
__date__ = "$Date: 2016.04.30 $"
__copyright__ = "Copyright (c) 2016 Sam Nirvana"
__license__ = "GNU GPL 3+"

# Every statement in the ``markup`` output has to start
# with one of the following tags.
tags = ('@begin docs',
        '@end docs',
        '@begin code',
        '@end code',
        '@text',
        '@nl',
        '@defn',
        '@use',
        '@quote',
        '@endquote',
        '@file',
        '@line',
        '@language',
        '@index defn',
        '@index localdefn',
        '@index use',
        '@index nl',
        '@index begindefs',
        '@index isused',
        '@index defitem',
        '@index enddefs',
        '@index beginuses',
        '@index isdefined',
        '@index useitem',
        '@index enduses',
        '@index beginindex',
        '@index entrybegin',
        '@index entryuse',
        '@index entrydefn',
        '@index entryend',
        '@index endindex',
        '@xref label',
        '@xref ref',
        '@xref notused',
        '@xref begindefs',
        '@xref enddefs',
        '@xref defitem',
        '@xref beginuses',
        '@xref enduses',
        '@xref useitem',
        '@xref prevdef',
        '@xref nextdef',
        '@xref beginchunks',
        '@xref chunkbegin',
        '@xref chunkuse',
        '@xref chunkdefn',
        '@xref chunkend',
        '@xref endchunks',
        '@xref tag',
        '@header',
        '@trailer',
        '@fatal',
        '@literal')

class Statement:
    """Every line of text coming from the ``markup`` pipeline is a Statement.
    
    Checks if the parameter is a well-formed ``markup`` statement and
    provides ``tag`` and ``argument`` properties.
    
    Attributes:
        statement (str): the line of text coming from
            the ``markup`` pipeline.
    """
    
    def __init__(self, statement):
        """Accept the parameter ``statement`` as a class attribute
            only if it is a well-formed ``markup`` statement.
        
        Args:
            statement (str): the line of text coming from
                the ``markup`` pipeline.
        """
        if not statement:
            raise ValueError("The 'Statement' class "
                             "requires a not-empty string parameter.")
        self.__check(statement)
        self.statement = statement
    
    def __check(self, a_statement):
        """Check if ``a_statement`` is a well-formed ``markup`` statement.
        
        ``a_statement`` is a well-formed ``markup`` statement if it
        starts with one of the tags in the ``tags`` tuple. If
        ``a_statement`` is not validated the program cannot go on, so it
        raises a ``ValueError``.
        
        Args:
            a_statement (str): a not-empty line of text coming from
                the ``markup`` pipeline.
        """
        statement_validated = False
        for tag in tags:
            if a_statement.startswith(tag):
                statement_validated = True
                break
        if not statement_validated:
            raise ValueError("The given parameter is not "
                             "a valid 'markup' statement.")
    
    @property
    def tag(self):
        """The first part of the statement as stated in the 'tags' tuple."""
        for tag in tags:
            if self.statement.startswith(tag):
                return tag
    
    @property
    def argument(self):
        """The argument of the 'markup' tag."""
        for tag in tags:
            if self.statement.startswith(tag):
                return self.statement.rsplit(tag+" ", 1)[1]



# import sys
#
# for line in sys.stdin.readlines():
#     pass

pass

