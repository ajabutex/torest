********
 torest
********

:author: Sam Nirvana <samnirvana@linuxmail.org> 
:version: $Revision: 0.0.0 $
:date: $Date: 2016.04.13 $
:description: Python script that has to be used as a ``noweb`` filter
              to output documentation written in ReST.

..

 This file is part of torest.
 torest is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 torest is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with torest.  If not, see <http://www.gnu.org/licenses/>.

.. contents:: Table of Contents

.. sectnum::


Preliminaries
=============


What is it
----------

``torest`` is a filter for ``noweb``. It weave out the documentation
part of a program written in the way of *Literate Programming*. It is
useful if the documentation is written in restructuredtext.


What is the *Literate Programming*
----------------------------------

  "*Literate programming* is a methodology that combines a programming
  language with a documentation language, thereby making programs more
  robust, more portable, more easily maintained, and arguably more fun
  to write than programs that are written only in a high-level language.

  The main idea is to treat a program as a piece of literature,
  addressed to human beings rather than to a computer." [Knuth]_

The main web reference about this topic is the `Literate Programming
site <http://www.literateprogramming.com>`_.

.. [Knuth] Donald Knuth: *Literate Programming*. Stanford,
   California: Center for the Study of Language and Information, 1992.


What is noweb
-------------

  "``noweb`` is a literate programming tool, created in 1989-1999 by
  `Norman Ramsey <https://www.cs.tufts.edu/~nr/noweb/>`_, and designed
  to be simple, easily extensible and language independent.

  As in WEB and CWEB, the main components of ``noweb`` are two programs:
  *notangle*, which extracts 'machine' source code from the source
  texts, and *noweave*, which produces nicely-formatted printable
  documentation.

  ``noweb`` supports TeX, LaTeX, HTML, and troff back ends and works
  with any programming language. Besides simplicity this is the main
  advantage over WEB, which needs different versions to support
  programming languages other than Pascal." (`Wikipedia
  <https://en.wikipedia.org/wiki/Noweb>`_)


The (no)tangling pipe
.....................

  "``noweb`` is unique among literate-programming tools in its pipelined
  architecture, which makes it easy for users to change its behavior or
  to add new features, without even recompiling. [...]
  
  The ``noweb`` tools are implemented as pipelines. Each pipeline begins
  with the noweb source file. Successive stages of the pipeline
  implement simple transformations of the source, until the desired
  result emerges from the end of the pipeline.

  ``Markup``, which is the first stage in every pipeline, converts noweb
  source to a representation easily manipulated by common Unix tools
  like ``sed`` and ``awk``, simplifying the construction of later pipeline
  stages. Middle stages add information to the representation.
  notangle’s final stage converts to code; noweave’s final stages
  convert to TeX, LaTeX or HTML. Middle stages are called filters, by
  analogy with Unix filters." [Ramsey]_

.. [Ramsey] Norman Ramsey, *The `noweb` Hacker's Guide*, 1992-1997. It's
   worth `taking a look at it
   <https://www.cs.tufts.edu/~nr/noweb/guide.ps>`_, so as at `this page
   <https://www.cs.tufts.edu/~nr/noweb/extensibility.html>`_ that
   explain the noweb's extensibility strategy.


The pipe at work
................

The best way to understand how it work ``noweb`` and ``torest`` is to
see the pipe as it unfolds. Let's take a little ``noweb`` file, written
with ReST and Python. The file name is ``pipetest.nw``::

  =========
  pipe test
  =========
  
  A little benchmark to study the behavior of noweb.
  
  .. contents:: Table of Contents
  
  The name of this program is "pipe" and has two blocks
  of code.
  
  <<pipe>>=
    <<Add two terms>>
    <<Say hallo>>
  
  @ The first block of code takes two parameters and return the sum of
  them.
  
  <<Add two terms>>=
  def add(a, b):
    result = a + b
    return result
  @ %def add
  
  @ The second and last block of code takes one parameter, uses the
  ``add`` function previously defined, and finally say hallo.
  
  <<Say hallo>>=
  def sayhallo(s):
    v = add(s, s+1)
    print "Hallo %d" % (v)
  @ %def sayhallo
  
  @ So has come the end of the program.
  
  .. nowebchunks:: List of chunks
  
  .. nowebindex:: List of identifiers
  
We'll execute and follow the entire pipe of ``noweave`` step by step.
The stages that we'll consider are::

  file .nw  ->  .mrk    ->  .fnd ->  .ndx  ->   .rst
          markup -> finduses -> noidx -> torest

Now let's begin pipeing ``pipetest.nw`` trough ``markup``, the first
stage of the ``noweave`` sequence. The ``stdin`` is redirected into
``pipetest.mrk``.

::

  sam@lap $ /usr/lib/noweb/markup pipetest.nw > pipetest.mrk

Take a look at ``pipetest.mrk``. You will learn a lot about the
mechanics of ``noweb``.

Now the second stage: let's pipe ``pipetest.mrk`` trough ``finduses``.
The ``stdin`` is redirected into ``pipetest.fnd``.

::

  sam@lap $ cat pipetest.mrk | /usr/lib/noweb/finduses > pipetest.fnd

And finally the third stage, the one with which we'll obtain
``pipetest.ndx``, the file with all the informations for create a
restructuredtext version of our literate program::

  sam@lap $ cat pipetest.fnd | /usr/lib/noweb/noidx > pipetest.ndx
  

A fast look at ``pipetest.ndx``
...............................

Let's try to understand the structure of ``pipetest.ndx``::

  @file pipetest.nw
  @begin docs 0
  @text =========
  @nl
  @text pipe test
  @nl
  @text =========
  @nl
  @text 
  @nl
  @text A little benchmark to study the behavior of noweb.
  @nl
  @text 
  @nl
  @text .. contents:: Table of Contents
  @nl
  @text 
  @nl
  @text The name of this program is "pipe" and has two blocks
  @nl
  @text of code.
  @nl
  @text 
  @nl
  @end docs 0
  @begin code 1
  @xref label NWB3Pa9-4Fysxb-1
  @xref ref NWB3Pa9-4Fysxb-1
  @defn pipe
  @xref notused pipe
  @nl
  @text   
  @xref label NWB3Pa9-4Fysxb-1-u1
  @xref ref NWB3Pa9-5ZRIx-1
  @use Add two terms
  @nl
  @text   
  @xref label NWB3Pa9-4Fysxb-1-u2
  @xref ref NWB3Pa9-2YmUPg-1
  @use Say hallo
  @nl
  @nl
  @end code 1
  

The tag ``@nl`` stands simply for a newline char.

Every block of documentation begin with ``@begin docs`` and a
sequential number and end with ``@end docs`` with the same number.

Every chunk of code begin with ``@begin code #`` and end with ``@end
code #``.



















What does it do *torest*
------------------------


Installation
------------


Use
---



.. .....................................................................



The program
===========




