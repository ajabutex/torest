=========
pipe test
=========

A little benchmark to study the behavior of noweb.

.. contents:: Table of Contents

.. Table of Contents
.. -----------------
..
.. -  `List of chunks <#list-of-chunks>`__
.. -  `List of identifiers <#list-of-identifiers>`__

The name of this program is "pipe" and has two blocks of code.

::

  <pipe {1}>=
    <Add two terms {2}>
    <Say hallo {3}>

The first block of code takes two parameters and return the sum of
them. Think of it as a ``graeting code`` between two column of beer.

::

  <Add two terms {2}>=
    def add(a, b):
        result = a + b
        return result
  [defines <add> used in chunk {3}]

then open the list and return the result.

::

  <Add two terms {2}>+=
    result = result - []
    return result

The second and last block of code takes one parameter, uses the add
function previously defined, and finally say hallo.

::

  <Say hallo {3}>=
    def sayhallo(s):
      v = add(s, s+1)
      print "Hallo %d" % (v)
  [defines <sayhallo>]

So has come the end of the program.

List of chunks
--------------

- ``<pipe>``, defined at ``{1}``
- ``<Add two terms>``, defined at ``{2}``, used at ``{1}``
- ``<Say hallo>``, defined at ``{3}``, used at ``{1}``

List of identifiers
-------------------

-  *add*, defined at ``{2}``, used at ``{3}``
-  *sayhallo*, defined at ``{3}``

